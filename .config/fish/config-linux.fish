if type -q exa
	alias ls "exa --icons"
	alias ll "exa -l -g --icons"
	alias la "ll -a"
end
